from flask import Flask, request, jsonify
import db
import pymysql

app = Flask(__name__)

@app.route("/cargos/<int:codigo>", methods=['GET'])
def get_cargo(codigo):
    con = db.get_connection() 
    cursor = con.cursor(pymysql.cursors.DictCursor)
    retorno={}
    try:
        sql="SELECT * FROM cargos WHERE codigo = {}".format(codigo)
        cursor.execute(sql)
        retorno = cursor.fetchone()
        return jsonify(retorno)
    finally:
        con.close()
        print("Conexion cerrada")

@app.route("/cargos", methods=['GET'])
def get_cargos():
    con = db.get_connection()
    cursor = con.cursor(pymysql.cursors.DictCursor)
    try:
        sql="SELECT * FROM cargos"
        cursor.execute(sql)
        retorno = cursor.fetchall()
        return jsonify(retorno)
    finally:
        con.close()
        print("Conexion cerrada")

@app.route("/cargos", methods=['POST'])
def create():
    data = request.get_json()
    nombre = data['nombre']
    con = db.get_connection()
    cursor = con.cursor()
    try:
        sql="INSERT INTO cargos(nombre) VALUES('{}')".format(nombre)
        cursor.execute(sql)
        con.commit()
        return jsonify({"mensaje":"OK"})
    finally:
        con.close()
        print("Conexion cerrada")

@app.route("/cargos/<int:codigo>", methods=['PUT'])
def update(codigo):
    data = request.get_json()
    nombre = data['nombre']
    con = db.get_connection()
    cursor = con.cursor()
    try:
        sql="UPDATE cargos set nombre='{0}' WHERE codigo = {1}".format(nombre, codigo)
        cursor.execute(sql)
        con.commit()
        return jsonify({"mensaje":"OK"})
    finally:
        con.close()
        print("Conexion cerrada")

@app.route("/cargos/<int:codigo>", methods=['DELETE'])
def delete(codigo):
    con = db.get_connection()
    cursor = con.cursor()
    try:
        sql="DELETE FROM cargos WHERE codigo = {}".format(codigo)
        cursor.execute(sql)
        con.commit()
        return jsonify({"mensaje":"OK"})
    finally:
        con.close()
        print("Conexion cerrada")
